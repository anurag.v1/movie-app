package com.example.newmovieapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


class MoviesAdapter(private val movies: ArrayList<Movie>)
    :RecyclerView.Adapter<MoviesAdapter.MovieViewHolder>(){
    var onItemClick: ((Movie)->Unit)?=null
    class MovieViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val moviePoster: ImageView=itemView.findViewById(R.id.movieView)
        val movieTitle: TextView=itemView.findViewById(R.id.textView2)
        val ratingBar: RatingBar=itemView.findViewById(R.id.ratingBar)
        val relDate: TextView=itemView.findViewById(R.id.textView3)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.movie_item,parent,false)
        return MovieViewHolder(view)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movieList=movies[position]
        holder.moviePoster.setImageResource(movieList.image)
        holder.movieTitle.text=movieList.title
        holder.ratingBar.rating=movieList.rating
        holder.relDate.text=movieList.releaseDate
        holder.itemView.setOnClickListener {
             onItemClick?.invoke(movieList)
        }
    }
}