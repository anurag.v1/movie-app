package com.example.newmovieapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

private lateinit var recyclerView: RecyclerView
private lateinit var recyclerView2: RecyclerView
private lateinit var movieList : ArrayList<Movie>
private lateinit var movieList2 : ArrayList<Movie>
private lateinit var moviesAdapter: MoviesAdapter
private lateinit var moviesAdapter2: MoviesAdapter
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView =findViewById(R.id.recyclerView)
        recyclerView2=findViewById(R.id.recyclerView2)

        movieList= ArrayList<Movie>()
        movieList2=ArrayList<Movie>()

        movieList.add(Movie("Movie 1",R.drawable.movie1,4f,"14 Jan 2002","\n" +
                "A genre is a specific type of music, film, or writing. Your favorite literary genre might be science fiction, and your favorite film genre might be horror flicks about cheerleaders. Go figure. In music, genre refers to musical style such as jazz, salsa or rock."))
        movieList.add(Movie("Movie 2",R.drawable.movie2,3f,"18 Aug 2005","\n" +
                "A genre is a specific type of music, film, or writing. Your favorite literary genre might be science fiction, and your favorite film genre might be horror flicks about cheerleaders. Go figure. In music, genre refers to musical style such as jazz, salsa or rock."))
        movieList.add(Movie("Movie 3",R.drawable.movie3,3.5f,"20 May 2007","\n" +
                "A genre is a specific type of music, film, or writing. Your favorite literary genre might be science fiction, and your favorite film genre might be horror flicks about cheerleaders. Go figure. In music, genre refers to musical style such as jazz, salsa or rock."))
        movieList.add(Movie("Movie 4",R.drawable.movie4,4.5f,"5 March 2009","\n" +
                "A genre is a specific type of music, film, or writing. Your favorite literary genre might be science fiction, and your favorite film genre might be horror flicks about cheerleaders. Go figure. In music, genre refers to musical style such as jazz, salsa or rock."))
        movieList.add(Movie("Movie 5",R.drawable.movie5,4f,"19 July 2010","\n" +
                "A genre is a specific type of music, film, or writing. Your favorite literary genre might be science fiction, and your favorite film genre might be horror flicks about cheerleaders. Go figure. In music, genre refers to musical style such as jazz, salsa or rock."))
        movieList.add(Movie("Movie 6",R.drawable.movie6,3f,"6 Dec 2011","\n" +
                "A genre is a specific type of music, film, or writing. Your favorite literary genre might be science fiction, and your favorite film genre might be horror flicks about cheerleaders. Go figure. In music, genre refers to musical style such as jazz, salsa or rock."))
        movieList.add(Movie("Movie 7",R.drawable.movie7,2f,"1 June 2013","\n" +
                "A genre is a specific type of music, film, or writing. Your favorite literary genre might be science fiction, and your favorite film genre might be horror flicks about cheerleaders. Go figure. In music, genre refers to musical style such as jazz, salsa or rock."))
        movieList.add(Movie("Movie 8",R.drawable.movie8,4f,"1 Nov 2015","\n" +
                "A genre is a specific type of music, film, or writing. Your favorite literary genre might be science fiction, and your favorite film genre might be horror flicks about cheerleaders. Go figure. In music, genre refers to musical style such as jazz, salsa or rock."))
        movieList.add(Movie("Movie 9",R.drawable.movie9,3.5f,"21 Oct 2006","\n" +
                "A genre is a specific type of music, film, or writing. Your favorite literary genre might be science fiction, and your favorite film genre might be horror flicks about cheerleaders. Go figure. In music, genre refers to musical style such as jazz, salsa or rock."))
        movieList.add(Movie("Movie 10",R.drawable.movie10,1.5f,"9 Sept 2011","\n" +
                "A genre is a specific type of music, film, or writing. Your favorite literary genre might be science fiction, and your favorite film genre might be horror flicks about cheerleaders. Go figure. In music, genre refers to musical style such as jazz, salsa or rock."))
        movieList.add(Movie("Movie 11",R.drawable.movie11,3.5f,"14 Feb 2019","\n" +
                "A genre is a specific type of music, film, or writing. Your favorite literary genre might be science fiction, and your favorite film genre might be horror flicks about cheerleaders. Go figure. In music, genre refers to musical style such as jazz, salsa or rock."))
        movieList.add(Movie("Movie 12",R.drawable.movie12,5f,"19 April 2013","\n" +
                "A genre is a specific type of music, film, or writing. Your favorite literary genre might be science fiction, and your favorite film genre might be horror flicks about cheerleaders. Go figure. In music, genre refers to musical style such as jazz, salsa or rock."))

        movieList2.add(Movie("Movie 13",R.drawable.movie13,2f,"4 May 2003","\n" +
                "A genre is a specific type of music, film, or writing. Your favorite literary genre might be science fiction, and your favorite film genre might be horror flicks about cheerleaders. Go figure. In music, genre refers to musical style such as jazz, salsa or rock."))
        movieList2.add(Movie("Movie 14",R.drawable.movie14,4f,"5 July 2001","\n" +
                "A genre is a specific type of music, film, or writing. Your favorite literary genre might be science fiction, and your favorite film genre might be horror flicks about cheerleaders. Go figure. In music, genre refers to musical style such as jazz, salsa or rock."))
        movieList2.add(Movie("Movie 15",R.drawable.movie15,3f,"17 Aug 2020","\n" +
                "A genre is a specific type of music, film, or writing. Your favorite literary genre might be science fiction, and your favorite film genre might be horror flicks about cheerleaders. Go figure. In music, genre refers to musical style such as jazz, salsa or rock."))
        movieList2.add(Movie("Movie 16",R.drawable.movie16,3.5f,"10 April 2011","\n" +
                "A genre is a specific type of music, film, or writing. Your favorite literary genre might be science fiction, and your favorite film genre might be horror flicks about cheerleaders. Go figure. In music, genre refers to musical style such as jazz, salsa or rock."))
        movieList2.add(Movie("Movie 17",R.drawable.movie17,1f,"11 Dec 2021","\n" +
                "A genre is a specific type of music, film, or writing. Your favorite literary genre might be science fiction, and your favorite film genre might be horror flicks about cheerleaders. Go figure. In music, genre refers to musical style such as jazz, salsa or rock."))
        movieList2.add(Movie("Movie 18",R.drawable.movie18,4.5f,"6 Jan 2015","\n" +
                "A genre is a specific type of music, film, or writing. Your favorite literary genre might be science fiction, and your favorite film genre might be horror flicks about cheerleaders. Go figure. In music, genre refers to musical style such as jazz, salsa or rock."))
        movieList2.add(Movie("Movie 19",R.drawable.movie19,5f,"25 Sept 2017","\n" +
                "A genre is a specific type of music, film, or writing. Your favorite literary genre might be science fiction, and your favorite film genre might be horror flicks about cheerleaders. Go figure. In music, genre refers to musical style such as jazz, salsa or rock."))
        moviesAdapter= MoviesAdapter(movieList)
        moviesAdapter2= MoviesAdapter(movieList2)
        recyclerView.layoutManager = LinearLayoutManager(this,RecyclerView.HORIZONTAL,false)
        recyclerView.adapter=moviesAdapter
        recyclerView2.layoutManager = LinearLayoutManager(this,RecyclerView.HORIZONTAL,false)
        recyclerView2.adapter=moviesAdapter2

        moviesAdapter.onItemClick={
            val intent=Intent(this,Detailed_Activity::class.java)
            intent.putExtra("movie",it)
            startActivity(intent)
        }
        moviesAdapter2.onItemClick={
            val intent=Intent(this,Detailed_Activity::class.java)
            intent.putExtra("movie",it)
            startActivity(intent)
        }

    }

}