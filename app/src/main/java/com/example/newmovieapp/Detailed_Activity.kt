package com.example.newmovieapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView

class Detailed_Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.movie_desc)
        val movie=intent.getParcelableExtra<Movie>("movie")
        if(movie!=null){
            val textView : TextView=findViewById(R.id.textView5)
            val imageView: ImageView=findViewById(R.id.imageView2)
            val textView2: TextView=findViewById(R.id.textView6)
            textView.text=movie.title
            imageView.setImageResource(movie.image)
            textView2.text=movie.desc
        }
    }
}